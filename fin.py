def digit_occurrences(start, end, digit):
	s = []
	t=0
	for i in range(start, end+1):
		s.append(i)
	for i in s:
		if abs(i)>99 and (int((abs(i)/10)%10) == digit and int(abs(i)/100) == digit and int(abs(i)%10) == digit):
			t=t+3
		elif abs(i)>99 and (int((abs(i)/10)%10) != digit and int(abs(i)/100) == digit and int(abs(i)%10) == digit):
			t=t+2
		elif abs(i)>99 and (int((abs(i)/10)%10) == digit and int(abs(i)/100) != digit and int(abs(i)%10) == digit):
			t=t+2
		elif abs(i)>99 and (int((abs(i)/10)%10) == digit and int(abs(i)/100) == digit and int(abs(i)%10) != digit):
			t=t+2
		elif abs(i)>99 and (int(abs(i)/100) == digit or int((abs(i)/10)%10) == digit or int(abs(i)%10) == digit):
			t=t+1
		elif 9<abs(i)<100 and (int(abs(i)%10) == digit and int(abs(i)/10) == digit):
			t=t+2
		elif 9<abs(i)<100 and ((abs(i)%10) == digit): 
			t=t+1
		elif abs(i) == digit:
			t=t+1
		elif 9<abs(i)<100 and (int(abs(i)/10) == digit):
			t=t+1
		else:
			t=t
	return t

print(digit_occurrences(51, 55, 5))
print(digit_occurrences(1, 8, 9))
print(digit_occurrences(71, 77, 2))
print(digit_occurrences(1, 14, 4))
print(digit_occurrences(20, 30, 2))
print(digit_occurrences(18, 37, 3))
print(digit_occurrences(5, 335, 6))
print(digit_occurrences(-19, 19, 0))
print(digit_occurrences(-8, -1, 8))
print(digit_occurrences(-5, -5, 4))
print(digit_occurrences(-5, -5, 5))
print(digit_occurrences(-50, -45, 4))
print(digit_occurrences(-500, -45, 4))