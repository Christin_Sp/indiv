import fin

def assert_equals(start, end, digit, result):
	print('start test, interval: [' + str(start)+ ', '+ str(end) + ']\ndigit to check: ' + str(digit) + '\nresult: ' + str(result))
	if (fin.digit_occurrences(start, end, digit) == result):
		print('function work')
	else:
		print('function not work')

assert_equals(51, 55, 5, 6)
assert_equals(1, 8, 9, 0)
assert_equals(71, 77, 2, 1)
assert_equals(1, 14, 4, 2)
assert_equals(20, 30, 2, 11)
assert_equals(18, 37, 3, 10)
assert_equals(5, 335, 6, 63)
assert_equals(-19, 19, 0, 3)
assert_equals(-8, -1, 8, 1)
assert_equals(-5, -5, 4, 0)
assert_equals(-5, -5, 5, 1)
assert_equals(-50, -45, 4, 5)
assert_equals(-500, -45, 4, 190)