def SortSimbl(example):
	if example == '':
		return True
	else:
		if len(example)%2 != 0:
			return False;
		elif len(example)==2:
			if example[0]=='[' and example[1]==']':
				return True
			elif example[0]=='{' and example[1]=='}':
				return True
			elif example[0]=='(' and example[1]==')':
				return True
			else:
				return False
		elif len(example)==4:
			if (example[0]=='{' and example[-1]=='}') or (example[0]=='(' and example[-1]==')') or (example[0]=='[' and example[-1]==']'):
				if (example[+1]=='{' and example[-2]=='}') or (example[1]=='(' and example[-2]==')') or (example[1]=='[' and example[-2]==']'):
					return True
			elif (example[0]=='[' and example[1]==']') or(example[0]=='(' and example[1]==')') or (example[0]=='{' and example[1]=='}'):
				if (example[2]=='[' and example[3]==']') or(example[2]=='(' and example[3]==')') or (example[2]=='{' and example[3]=='}'):
					return True
			else:
				return False
		elif len(example)==6:
			if (example[0]=='{' and example[-1]=='}') or (example[0]=='(' and example[-1]==')') or (example[0]=='[' and example[-1]==']'):
				if ((example[1]=='{' and example[-2]=='}') or (example[1]=='(' and example[-2]==')') or (example[1]=='[' and example[-2]==']')):
					if (example[2]=='{' and example[-3]=='}') or (example[2]=='(' and example[-3]==')') or (example[2]=='[' and example[-3]==']'):
						return True
			elif (example[0]=='[' and example[1]==']') or(example[0]=='(' and example[1]==')') or (example[0]=='{' and example[1]=='}'):
				if (example[2]=='[' and example[3]==']') or(example[2]=='(' and example[3]==')') or (example[2]=='{' and example[3]=='}') or ((example[2]=='{' and example[-1]=='}') or (example[2]=='(' and example[-1]==')') or (example[1]=='[' and example[-1]==']')):
					if ((example[4]=='[' and example[5]==']') or(example[4]=='(' and example[5]==')') or (example[4]=='{' and example[5]=='}')) or ((example[3]=='{' and example[-2]=='}') or (example[3]=='(' and example[-2]==')') or (example[3]=='[' and example[-2]==']')):
						return True
			elif (example[0]=='[' and example[3]==']') or(example[0]=='(' and example[3]==')') or (example[0]=='{' and example[3]=='}'):
				if (example[1]=='{' and example[2]=='}') or (example[1]=='(' and example[2]==')') or (example[1]=='[' and example[2]==']'):
					if (example[4]=='{' and example[5]=='}') or (example[4]=='(' and example[5]==')') or (example[4]=='[' and example[5]==']'):
						return True
			else:
				return False

print("Example 1:\nInput:\"()\"\nOutput:", SortSimbl("()"))
print("Example 2:\nInput:\"()[]{}\"\nOutput:", SortSimbl("()[]{}"))
print("Example 3:\nInput:\"(]\"\nOutput:", SortSimbl("(]"))
print("Example 4:\nInput:\"([)]\"\nOutput:", SortSimbl("([)]"))
print("Example 5:\nInput:\"{[]}\"\nOutput:", SortSimbl("{[]}"))

"""print(SortSimbl(""))
print(SortSimbl("("))
print(SortSimbl("()"))
print(SortSimbl("{()}"))
print(SortSimbl("{}()"))
print(SortSimbl("{}()[]"))
print(SortSimbl("({[]})"))
print(SortSimbl("(){[]}"))
print(SortSimbl("{[]}()"))"""